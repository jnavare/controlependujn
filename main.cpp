#include <irrlicht.h>
#include "Game.h"
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <time.h>

using namespace irr;


using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;


int main(int argc, char** argv)
{
    //initialisation du random
    srand(time(NULL));

    //Classe principale du jeu
	Game *pendu = new Game();

	//Boucle principale
    pendu->run();

    //Destructeur
    delete pendu;
}
