#ifndef GAMEGUI_H
#define GAMEGUI_H

#include <irrlicht.h>
#include <iostream>
#include <string>
#include "enums.h"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

class GameGUI
{
    public:
        GameGUI(IGUIEnvironment *guienv);
        virtual ~GameGUI();

        IGUIStaticText *fenetreJeu;

        //Fenetre Jeu
        IGUIStaticText *labelEssais;
        IGUIStaticText *labelNbEssais;
        IGUIStaticText *labelChoixLettre;
        IGUIStaticText *labelMotMystere;
        IGUIButton *btnDebutPartie;
        IGUIButton *btnValider;


        irr::gui::IGUIEditBox *choixLettre;
    protected:

    private:
        IGUIEnvironment *guienv;


};

#endif // GAMEGUI_H
